/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author Rubinho
 */
public class ContadorPalavras {
    private BufferedReader reader;
    private File arquivo;
    HashMap<String,Integer> example = new HashMap<>();

    public ContadorPalavras(String file) throws IOException{
        arquivo = new File(file);
    }
    
    public HashMap getPalavras(){
        try {
            if (!arquivo.exists()) {
                //cria um arquivo (vazio)
                arquivo.createNewFile();
            }
            //A classe FileReader recebe como argumento o objeto File do arquivo a ser lido:
            //construtor que recebe o objeto do tipo arquivo
            FileReader fr = new FileReader(arquivo);
            //construtor que recebe o objeto do tipo FileReader
            reader = new BufferedReader(fr);
            //equanto houver mais linhas
            while( reader.ready() ){
                //lê a proxima linha
                String frase = reader.readLine();
                String[] palavras = frase.split(" ");
//                System.out.println(palavras.length);
//                System.out.println(frase);
                for (String word: palavras){
//                    System.out.println(word);
                    for (String key : example.keySet()) {
                        if (word.equals(key)){
                            //Capturamos o valor a partir da chave
                            Integer value = example.get(key);
                            example.put(key, value+1);
//                            value = example.get(key);
//                            System.out.println(key + " = " + value);
                        }
                    }
                    if (!example.containsKey(word)){
                        example.put(word, 1);
//                        System.out.println(word + " = ");
                    }
                    if (example.containsKey("")){
                        example.remove("");
                    }
                }        
            }
            reader.close();
            fr.close();
        }catch (IOException ex){
        }
        return example;
    }

    public void createCsvFile(String caminho, HashMap<String,Integer> texto){ 
        File arquivo = new File( caminho );
        //verifica se o arquivo ou diretório existe
        boolean existe = arquivo.exists();

        //A estrutura try-catch é usada pois o objeto BufferedWriter exige que as
        //excessões sejam tratadas
        try{
            if (!arquivo.exists()) {
                //cria um arquivo (vazio)
                arquivo.createNewFile();
            }else{
                arquivo.delete();
                arquivo.createNewFile();
            }
            FileWriter fw = new FileWriter( arquivo, true );
            //Criação de um buffer para a escrita em uma stream
            BufferedWriter StrW = new BufferedWriter(fw);
            
            //Escrita dos dados da tabela
            for (String key : texto.keySet()) {
                //Capturamos o valor a partir da chave
                Integer value = texto.get(key);
//                System.out.println(key + " = " + value);
                StrW.write(key+";"+value+"\n"); 
            }

            //Fechamos o buffer
            StrW.close(); 
        }catch (FileNotFoundException ex){
        }catch (IOException e){
        } 
    }
}