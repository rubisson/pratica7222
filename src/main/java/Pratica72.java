
import java.io.IOException;
import java.util.HashMap;
import utfpr.ct.dainf.if62c.pratica.ContadorPalavras;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica72 {
    public static void main(String[] args) throws IOException {
        ContadorPalavras pal = new ContadorPalavras("C:\\Users\\Rubinho\\Documents\\NetBeansProjects\\Pratica72\\arqTest.txt");
        HashMap<String,Integer> mapWord = new HashMap<>();        
        mapWord = pal.getPalavras();
/*        
        for (String key : mapWord.keySet()) {
            //Capturamos o valor a partir da chave
            Integer value = mapWord.get(key);
            System.out.println(key + " = " + value);
        }
        System.out.println("-----------------------------------------------------");*/
        pal.createCsvFile("C:\\Users\\Rubinho\\Documents\\NetBeansProjects\\Pratica72\\arqTest.txt.out.csv", mapWord);
        

/*        HashMap<String,Integer> example = new HashMap<>();

        String frase = "era uma vez, um programador";
	String[] palavras = frase.split(" ");
	System.out.println(palavras.length);
        for (String word: palavras){
            System.out.println(word);
        }        
    
        example.put("K1", 10);
        example.put("K2", 1);
        System.out.println(" " + example.get("K1"));
        System.out.println(" " + example.containsKey("K1"));
        System.out.println(" " + example.containsValue(1));
        System.out.println(" " + example.size());
        System.out.println("------------------------------");
        example.put("K1", 11);
        for (String key : example.keySet()) {
            //Capturamos o valor a partir da chave
            Integer value = example.get(key);
            System.out.println(key + " = " + value);
            
        }
*/
    }
}
